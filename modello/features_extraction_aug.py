import os
import sys
import json
import time
import shutil
import pathlib
import librosa
import fnmatch
import numpy as np
import librosa.display
from multiprocessing import Pool
from audio_processing import split_silence, audio_mixing

# Paths
mel_path = "mel_img/"
audio_path = "audio_files"
aug_path = "aug_img/"

# Features extraction settings
concurrent = True  # Toggle multi-process
augment_data = False  # Toggle data augmentation
split_audio = False  # Toggle audio splitting on silence
audio_mix = False  # Toggle audio mixing

dataset_size = 0
extracted_covid = 0
extracted_nocovid = 0

def extractMelSpectogram(saveDir, uid, f="", mono=True, dpi=80):
    import matplotlib.pyplot as plt
    if f == "":
        raise Exception("No file specified")

    if saveDir == "":
        raise Exception("No out path specified")

    if not os.path.exists(f):
        raise Exception("File not found")

    try:
        audio, sr = librosa.load(f, mono=mono)
    except Exception as e:
        print(e)
        return

    melspec = librosa.feature.melspectrogram(y=audio, sr=sr, n_fft=2048, n_mels=128, hop_length=512, window='hann')
    log_melspec = librosa.power_to_db(melspec, ref=np.max)
    librosa.display.specshow(log_melspec, sr=sr)
    out_path = saveDir + uid + '_spec.png'
    plt.savefig(out_path, dpi=dpi, bbox_inches='tight', transparent=True, pad_inches=0)
    plt.close()

    return out_path


def prepareMelExtraction(labelled_data):
    file = labelled_data[0]
    label = labelled_data[1]
    index = labelled_data[2]
    aug = labelled_data[3]
    file = file.replace("\\", "/")

    saveDir = ""

    if (aug):
        if label == 0:
            saveDir = aug_path + "nocovid/"
        else:
            saveDir = aug_path + "covid/"
    else:
        if label == 0:
            saveDir = mel_path + "nocovid/"
        else:
            saveDir = mel_path + "covid/"

    if audio_mix:
        file = audio_mixing(file, ambient_path="ambient")

    if split_audio:
        files = split_silence(file)

        for i in range(len(files)):
            f = files[i]
            extractMelSpectogram(saveDir, str(index) + "_" + str(i), f=f)
            
    else:
        extractMelSpectogram(saveDir, str(index), file)
        

if __name__ == "__main__":
    # Printing settings
    print("Concurrent extraction: " + str(concurrent))
    print("Audio_mix: " + str(audio_mix))
    print("")

    # Audio folder
    if os.path.exists(audio_path):
        shutil.rmtree(audio_path)

    # Spec destination
    if os.path.exists(mel_path):
        shutil.rmtree(mel_path)

    # Augmented spec folder
    if os.path.exists(aug_path):
        shutil.rmtree(aug_path)

    os.makedirs(audio_path)
    os.makedirs(mel_path)
    os.makedirs(mel_path + "/nocovid")
    os.makedirs(mel_path + "/covid")
    os.makedirs(aug_path)
    os.makedirs(aug_path + "/nocovid")
    os.makedirs(aug_path + "/covid")

    # Dataset Portability
    j = open('path.json')
    base_path = (json.load(j)["path"])

    labelled_data = []
    covid = []
    nocovid = []
    aug = [] # augmentation has only been applied to non covid data

    data_dir = pathlib.Path(base_path)
    
    # healthyandroidwithcough + healthywebwithcough 66 (264 with augmentation)
    # covidandroidwithcough + covidwebwithcough 54

    for a in (data_dir.glob('*/**/*cough*.wav')):
        if "aug" not in str(a):
            if ("healthyandroidwithcough" in str(a) or "healthywebwithcough" in str(a)):
                nocovid.append(a)
            elif ("covidandroidwithcough" in str(a) or "covidwebwithcough" in str(a)):
                covid.append(a)
        elif "aug" in str(a):
            if ("healthyandroidwithcough" in str(a) or "healthywebwithcough" in str(a) or "covidandroidwithcough" in str(a) or "covidwebwithcough" in str(a)):
                aug.append(a)

    print("\nFound " + str(len(covid)+len(nocovid)) + " files")
    print("Covid labels: " + str(len(covid)))
    print("Nocovid labels: " + str(len(nocovid)))
    print("Augmentation labels: " + str(len(aug)))

    for i in range(len(covid)):
        dest = audio_path + "/cough_" + str(i) + ".wav"
        shutil.copy(str(covid[i]), dest)
        labelled_data.append([dest, 1, i, False])

    for i in range(len(nocovid)):
        dest = audio_path + "/cough_" + str(i+len(covid)) + ".wav"
        shutil.copy(str(nocovid[i]), dest)
        labelled_data.append([dest, 0, i+len(covid), False])
    
    for i in range(len(aug)):
        dest = audio_path  + "/cough_" + str(i+len(covid)+len(nocovid)) + ".wav"
        shutil.copy(str(aug[i]), dest)
        labelled_data.append([dest, 0, i+len(covid)+len(nocovid), True])


    dataset_size = len(labelled_data)

    start_time = time.time()

    if not concurrent:
        # Single processing
        for d in labelled_data:
            sys.stdout.write("\rExtracting data... " + str(d[2]) + "/" + str(dataset_size))
            prepareMelExtraction(d)
            
    else:
        # Parallel processing
        print("\nExtracting all melspectrograms...")
        with Pool(os.cpu_count() - 1) as pool:
            processed = pool.map(prepareMelExtraction, labelled_data)
        pool.close()


    sys.stdout.flush()
    elapsed_time = time.time() - start_time

    print("\nFeatures extraction completed in " + time.strftime('%M:%S', time.gmtime(elapsed_time)) + " minutes")

    extraction_path = pathlib.Path(mel_path)

    print("covid spectrogram generated: " + str(len(fnmatch.filter(os.listdir(mel_path+"covid/"), '*.png'))))
    print("nocovid spectrogram generated: " + str(len(fnmatch.filter(os.listdir(mel_path+"nocovid/"), '*.png'))))

